@Grab('org.yaml:snakeyaml:1.17')
import org.yaml.snakeyaml.Yaml
import hudson.util.RemotingDiagnostics;
import hudson.FilePath
import hudson.remoting.Channel
import jenkins.model.Jenkins
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.util.stream.Collectors


for (slave in hudson.model.Hudson.instance.slaves) {
println(slave.name)
    workspace="${new File(__FILE__).parent}"
    script = "def proc = \'ls -1 ${workspace}'.execute(); proc.waitFor(); println proc.in.text";
    println RemotingDiagnostics.executeGroovy(script, slave.getChannel())
    String filePath = workspace + '/repo_configs/config.yaml'
    Channel agentChannel = Jenkins.instance.slaves.find { agent ->
    agent.name == slave.name
    }.channel
    String fileContents = ''
    new FilePath(agentChannel, filePath).read().with { is ->
    try {
        fileContents = new BufferedReader(
            new InputStreamReader(is, StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"))
            }
    finally {
        is.close()
    }
}
Yaml parser = new Yaml()
Map projects = parser.load(fileContents)
repos=projects.repos_data
println(repos)
for ( int i=0;i<repos.size();i++) {
reponames=repos.repo_name[i]
println(reponames)
for (int j=0;j<reponames.size();j++){
    multibranchPipelineJob(reponames[j]) {
    branchSources {
        branchSource {
            source{ bitbucket {
                credentialsId(repos.credentialsId[i])
                bitbucketServerUrl('https://bitbucket.org')
				println("I am bitbucket")
                repoOwner(repos.workspace[i])
				println("I am bitbucket")
                repository(reponames[j])
				println("I am bitbucket")
                traits {
                    bitbucketBranchDiscovery {
                        strategyId(0)
                    }
                }
            }}
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(50)
        }
    }
}
}
}
    }


