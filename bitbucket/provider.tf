terraform {
  required_providers {
    bitbucket = {
      source  = "zahiar/bitbucket"
      version = "1.0.1"
    }
  }
}

# Configure the Bitbucket Provider
provider "bitbucket" {}
