locals {
  config           = yamldecode(file("../repo_configs/config.yaml"))
  repo_config      = local.config.repos_data
  project_creation = { for prj_data in local.config.repos_data : "${prj_data.project_name}" => prj_data }

  repo_creation = { for repo_data in flatten([for k, v in local.repo_config :
    [for repo in local.repo_config[k].repo_name :
      {
        project_key  = v.project_key
        workspace    = v.workspace
        repository   = repo
        project_name = v.project_name
  }]]) : "${repo_data.project_key}/${repo_data.workspace}/${repo_data.repository}" => repo_data }
}
