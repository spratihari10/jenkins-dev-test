# Manage your repository
resource "bitbucket_repository" "infrastructurerepo" {
  for_each    = local.repo_creation
  workspace   = each.value.workspace
  name        = each.value.repository
  project_key = each.value.project_key
  is_private  = false
  fork_policy = "no_forks"
  depends_on = [
    bitbucket_project.bitbucket-project
  ]
  lifecycle {
    ignore_changes = [
      description, project_key
    ]
    # prevent_destroy = true
  }

}

# Manage your project
resource "bitbucket_project" "bitbucket-project" {
  for_each  = local.project_creation
  workspace = each.value.workspace
  name      = each.value.project_name
  key       = each.value.project_key
  lifecycle {
    ignore_changes = [
      name
    ]
  }
}
