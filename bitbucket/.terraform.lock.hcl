# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/zahiar/bitbucket" {
  version = "1.0.1"
  hashes = [
    "h1:S2QAu1+wQuZVnL1AnHWInvbadaIPWRtrdxna45hKmfs=",
    "zh:2fbabe86f8d8120c963b21703a5ddc48a4de9b4fd680e57da3f386638d13c6d7",
    "zh:3ae83e19763290f0dee4d1facaab27b0f1de1ffc063fe442a0b7330bcb02fecc",
    "zh:4780d54c1488263a489d8cdf0f92fd8f203a2756562e8e1aa4ba24c0e2f5618c",
    "zh:75bbdc88373532f221092d19d08af87c4f6002d2458f4525a339b783ad8066d8",
    "zh:7634e55ddafc91351cfd605efa5a4386cd468d1051e020411f1b395964a1849d",
    "zh:8d8222b2c594a29682f6144e297cd9818f51ccc9d724ff1391a38dc8377f0101",
    "zh:b90676f6339fef85d957e50e1abb6172c15d27e6e81b2903217b2ce9316403f5",
    "zh:ba37ffa2c12e77fd74247f9141f1911fc4ffb76813a63a3a4437a524241a660f",
    "zh:c7dce8a4eae64c82c440dd89726aad49198a9396ecff4e0340830540d7ca08f6",
    "zh:d84f0a4db3cec14d4db2916e80cff175640c813c33336e87ac67026567820be4",
    "zh:ea284e84a049caf2a12e4faca22ba959900eae8bc19868980fc607a99b8eae5f",
    "zh:f014748b1102447c6d48ca5791c945ef700864f80fd1dce1db394d2abc2dd496",
  ]
}
